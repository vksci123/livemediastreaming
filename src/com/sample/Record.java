package com.sample;

import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore.Audio.Media;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.widget.Toast;

class RecorderPreview extends SurfaceView implements SurfaceHolder.Callback
{
  //Create objects for MediaRecorder and SurfaceHolder.
  MediaRecorder recorder;
  SurfaceHolder holder;

  //Create constructor of Preview Class. In this, get an object of
  //surfaceHolder class by calling getHolder() method. After that add
  //callback to the surfaceHolder. The callback will inform when surface is
  //created/changed/destroyed. Also set surface not to have its own buffers.

  	public RecorderPreview(Context context,MediaRecorder temprecorder) 
  	{
  		super(context);
  		recorder=temprecorder;
  		
  		holder=getHolder();
  		holder.addCallback(this);
  		holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
  	}

  // Implement the methods of SurfaceHolder.Callback interface

  // SurfaceCreated : This method gets called when surface is created.
  // In this, initialize all parameters of MediaRecorder object as explained
  // above.



public void surfaceCreated(SurfaceHolder holder){
  try{
	  String mfile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/myvideo.3gp" ;
	  String pat=Environment.getExternalStorageDirectory().getAbsolutePath() + "/File_2.3gp" ;
      Map<String, Integer> myMap = new HashMap<String, Integer>();
    //  Toast.makeText(Record.this,"Hello" + pat, Toast.LENGTH_SHORT).show() ;
     Integer i=(Integer)myMap.get(pat) ;
    //  MediaRecorder recorder=new MediaRecorder() ;
	  	
      recorder.setVideoSource(i.intValue()) ;//recorder.setVideoSource(Environment.getExternalStorageDirectory().getAbsolutePath() + "/File_2.3gp");
     
      recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
      recorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
      
      recorder.setOutputFile(mfile);
      recorder.setVideoFrameRate(10);
      recorder.setVideoSize(200, 300);
      
      

      recorder.setMaxDuration(50000);
      recorder.setMaxFileSize(5000000);
      
//   /   recorder.setPreviewDisplay(holder.getSurface());
       try
       {
    	   recorder.prepare();   
    	   recorder.start() ;
       }
     	
       catch (IllegalStateException e) {
           e.printStackTrace();
          // finish();
       } catch (IOException e) {
           e.printStackTrace();
          // finish();
       }

     	

      
      
    //  Toast.makeText(this,"Hello Sucessfully encoded", Toast.LENGTH_SHORT).show() ;
    	
    } catch (Exception e) {
    	String message = e.getMessage() ;
  }
}



public void surfaceDestroyed(SurfaceHolder holder)
{
    if(recorder!=null)
    {
	recorder.release();
	recorder = null;
    }
}

//surfaceChanged : This method is called after the surface is created.
public void surfaceChanged(SurfaceHolder holder, int format, int w, int h)
{
}
}


public class Record extends Activity
{
	
	
		
		private MediaRecorder recorder;
		private RecorderPreview preview;
		    // In this method, create an object of MediaRecorder class. Create an object of
		    // RecorderPreview class(Customized View). Add RecorderPreview class object
		    // as content of UI.

		public void onCreate(Bundle savedInstanceState)
		{
		    super.onCreate(savedInstanceState);
		    requestWindowFeature(Window.FEATURE_NO_TITLE);
		    recorder = new MediaRecorder();
		    preview = (RecorderPreview) new RecorderPreview(this,recorder);
		    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		    setContentView(preview);
		}

		//Create option menu which can be used for starting and stopping video recording.
		// Also implement the onOptionsItemSelected to handle the events for option menu.

		public boolean onCreateOptionsMenu(Menu menu)
		{
		    menu.add(0, 0, 0, "StartRecording");
		    menu.add(0, 1, 0, "StopRecording");
		    return super.onCreateOptionsMenu(menu);
		}

		public boolean onOptionsItemSelected(MenuItem item)
		{
		
		    switch (item.getItemId())
		    {
		        case 0:
		        	  try{
		        		  String mfile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/myvideo.3gp" ;
		        		  File mf=new File(mfile) ;
		        		  String pat=Environment.getExternalStorageDirectory().getAbsolutePath() + "/File_2.3gp" ;
		        	      Map<String, Integer> myMap = new HashMap<String, Integer>();
		        	      Toast.makeText(Record.this,"Hello" + pat, Toast.LENGTH_SHORT).show() ;
		        	     Integer i=(Integer)myMap.get(pat) ;
		        	    //  MediaRecorder recorder=new MediaRecorder() ;
		        	     Toast.makeText(this, "Recording not  started 2", Toast.LENGTH_SHORT).show() ;
		        	      recorder.setVideoSource(i.intValue()) ;//recorder.setVideoSource(Environment.getExternalStorageDirectory().getAbsolutePath() + "/File_2.3gp");
		        	      Toast.makeText(this, "Recording not  started 1", Toast.LENGTH_SHORT).show() ;
		        	      recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
		        	      recorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
		        	      
		        	      recorder.setOutputFile(mfile);
		        	      recorder.setVideoFrameRate(10);
		        	      recorder.setVideoSize(200, 300);
		        	      
		        	      

		        	      recorder.setMaxDuration(50000);
		        	      recorder.setMaxFileSize(5000000);
		        	      
		        	//   /   recorder.setPreviewDisplay(holder.getSurface());
		        	      Toast.makeText(this, "Recording not  started", Toast.LENGTH_SHORT).show() ;
		        	    	   recorder.prepare();   
		        	    	   //recorder.start() ;
		        	    	   Toast.makeText(this, "Recording started", Toast.LENGTH_SHORT).show() ;
		        	       
		        	     	
		        	      /* catch (IllegalStateException e) {
		        	           e.printStackTrace();
		        	          // finish();
		        	       } catch (IOException e) {
		        	           e.printStackTrace();
		        	          // finish();
		        	       }*/

		        	     	

		        	      
		   		        	recorder.start();

		        	    //  Toast.makeText(this,"Hello Sucessfully encoded", Toast.LENGTH_SHORT).show() ;
		        	    	
		        	    } catch (Exception e) {
		        	    	String message = e.getMessage() ;
		        	  }
		        	
		        	
		            break;
		        case 1:
			   recorder.stop();
			   break;
		    }
			
		    return super.onOptionsItemSelected(item);
		}
	}
	




