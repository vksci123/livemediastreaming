package com.sample;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import com.customvideo.*;
import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.VideoView;

public class SampleActivity extends Activity {
    /** Called when the activity is first created. */
	int i=0 ;
	String path="http://" ;
	MediaPlayer player ;
	Button b1;
	Button b2;
	Button b3;
	SeekBar pseek;
	private float progress ;
	Thread t1 ;
	Thread t2 ;
    boolean paused=false ;

	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
       b1=(Button) findViewById(R.id.button1) ;
    	 b2=(Button) findViewById(R.id.button2) ;
     b3=(Button) findViewById(R.id.button3) ;
     player=new MediaPlayer() ;
     pseek=(SeekBar) findViewById(R.id.seekBar1) ;
 	
	
 	
	
       
        path= path + getIntent().getExtras().getString("Ip");
        if(path!=null)
        {
        
        
        Toast.makeText(SampleActivity.this, "Ur path is "+path, Toast.LENGTH_SHORT).show() ;
        
        try {
        	Thread t=new Thread(new Runnable() {
				
				public void run() {
					// TODO Auto-generated method stub
					try {
						test() ;
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			});
			
        	t.start() ;
        }
        catch(Exception e)
        {}
        
        
        try{
        	
        	pseek.setMax(500) ;
    		
        	t1=new Thread(new Runnable() {
        		
        		
				public void run() {
					// TODO Auto-generated method stub
					progress=0 ;
					pseek.setProgress((int) progress) ;
					while(player.isPlaying())
			    	{
			    		try
			    		{
			    	    	progress = (((float)player.getCurrentPosition()/1000)/(float)214);
			    	    	pseek.setProgress((int)(progress*500));
			    		Log.d("Thread1","running") ;
			    		Thread.sleep(1000);
			    		}
			    		 catch (Exception e) {
			    	            return;
			    	        }
			    	        
			    		
			        }	
				}
			});
        	
        }
        catch(Exception e)
        {}
        
        try{
        	
        	pseek.setMax(500) ;
    		
        	t2=new Thread(new Runnable() {
        		
        		
				public void run() {
					// TODO Auto-generated method stub
					progress=0 ;
					pseek.setProgress((int) progress) ;
					while(player.isPlaying())
			    	{
			    		try
			    		{
			    	    	progress = (((float)player.getCurrentPosition()/1000)/(float)214);
			    	    	pseek.setProgress((int)(progress*500));
			    		Log.d("Thread1","running") ;
			    		Thread.sleep(1000);
			    		}
			    		 catch (Exception e) {
			    	            return;
			    	        }
			    	        
			    		
			        }	
				}
			});
        	
        }
        catch(Exception e)
        {}
        
        
        
        
        pseek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				
				// TODO Auto-generated method stub
				if(fromUser)
				{
					float diff=(float)player.getCurrentPosition()/1000-((progress)) ;
					Toast.makeText(SampleActivity.this,"This is "+(int)diff, Toast.LENGTH_SHORT).show() ;
					if((int)diff < 40)
					{
				player.seekTo((int)progress*500);
                pseek.setProgress((int)progress*500);
					}
				}
				
			}
		});
        
         
      /*  VideoView vv=(VideoView) findViewById(R.id.videoView1) ;
        
        
    	MediaController mediac=new MediaController(SampleActivity.this) ;
		mediac.setAnchorView(vv) ;
		
	
		vv.setVideoPath(path);//(Uri.parse(path)) ;
		mediac.setMediaPlayer(vv) ;
		vv.setMediaController(mediac) ;
		vv.requestFocus() ;
		vv.start() ;
		Toast.makeText(SampleActivity.this,"Hello Media player started successfully", Toast.LENGTH_SHORT).show() ;
		//int i=0 ;
		//for(i=0;i<20;i++)
		Toast.makeText(SampleActivity.this,"Hello Current pos is "+vv.getCurrentPosition(), Toast.LENGTH_SHORT).show() ;
		*/
        b1.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 try {
					 if(player != null)
			            {
			                if(!player.isPlaying() && !paused)
			                    {
			                	play() ;
			                	t1.start();
			                    }
			                else
			                    {
			                	player.start(); //if it was just paused
			                	t1.join() ;
			                    }
			            }
			            else
			            {
			                player = new MediaPlayer();
			                play() ;
			            }

					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalStateException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
			}
		});
        
        b2.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(player!= null && player.isPlaying())
	            {   
	                player.stop() ;
	                t1.interrupt();
	                t2.interrupt() ;
	                
	            }

				
			}
		});
        
        b3.setOnClickListener(new OnClickListener() {
			
    			public void onClick(View v) {
    				// TODO Auto-generated method stub
    				 try {
    					 player.stop();
    					 if(player != null)
    			            {
    			                if(!player.isPlaying() && !paused)
    			                    {
    			                	play() ;
    			                	t2.start();
    			                    }
    			                else
    			                    {
    			                	player.start(); //if it was just paused
    			                	
    			                    }
    			            }
    			            else
    			            {
    			                player = new MediaPlayer();
    			                play() ;
    			            }

    					} catch (IllegalArgumentException e) {
    						// TODO Auto-generated catch block
    						e.printStackTrace();
    					} catch (IllegalStateException e) {
    						// TODO Auto-generated catch block
    						e.printStackTrace();
    					} catch (IOException e) {
    						// TODO Auto-generated catch block
    						e.printStackTrace();
    					}
    			
    			}
    		});
       
		
        
        
   }
        
    }
    
    
    
    public void test() throws IOException, InterruptedException
    {
    	try
    	{
    //	String path="http://10.16.240.123:8080" ;
    	File extstore=Environment.getExternalStorageDirectory() ;
    	URL cn = new URL(path);
    	HttpURLConnection con=(HttpURLConnection) cn.openConnection() ;
    	InputStream stream = con.getInputStream();
    	if(stream==null)
    	{
    		Log.e("Error in stream", "Sorry cannot create a stream") ;
    	}
		//Toast.makeText(SampleActivity.this,"stream is created", Toast.LENGTH_SHORT).show() ;

        byte buf[]=new byte[162540];
        boolean flag=true ;
    	for(i=2;i<3;i++)
    	{
    		
    		File file1=new File(extstore.getAbsolutePath(),"File_"+ i + ".3gp");
    		FileOutputStream out=new FileOutputStream(file1) ;
    		int newread=0 ;
    		//int j=0 ;
    		do
    		{
    			newread=stream.read(buf) ;
    			while(newread==-1)
    			{
    				newread=stream.read(buf) ;
    			}
    		Log.i("Activity",Integer.toString(newread));
    		//out.write(buf,0,newread) ;
    		//j++ ;
    	//	TextView tv1=(TextView) findViewById(R.id.textView1) ;
    	//	tv1.setText(extstore.getAbsolutePath()) ;
    		if(newread==-1) 
    			{ flag=false ;
        	//	Toast.makeText(SampleActivity.this,"Reached end of the function", Toast.LENGTH_SHORT).show() ;
        		
    			}
    		else
    			out.write(buf,0,newread);
    		//Toast.makeText(SampleActivity.this,"Hello File"+ i+ " is being created", Toast.LENGTH_SHORT).show() ;
    		}while(flag) ;
    		stream.close() ;
    		out.close() ;
    		//Toast.makeText(SampleActivity.this,"Reached end of the function", Toast.LENGTH_SHORT).show() ;
    		
    		
    		
    		Log.e("SUccesfull ending","The video storing done successfully") ;
    	

    	}
    }
    	catch(Exception e)
        {
    	   e.printStackTrace();
    	   Log.e("Tagerror1","Caught in the catch block") ;
        	return ;
        }
    }
    
   
    
    
    
    public void play() throws IllegalArgumentException, IllegalStateException, IOException 
    {
    	player.reset();
    	player.setDataSource(path) ;
    	player.prepare() ;
    	
    	player.start();
    	
    }
    
    
  
 @Override
protected void onStop() {
	// TODO Auto-generated method stub
	 t1.interrupt() ;
	 t2.interrupt() ;
	super.onStop();
}
   
}






